package ru.tsc.denisturovsky.tm.command.system;

import java.util.Locale;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show developer info";

    public static final String FIRST_NAME = "Denis";

    public static final String LAST_NAME = "Turovsky";

    public static final String EMAIL = "dturovsky@t1-consulting.ru";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s %s \n", FIRST_NAME, LAST_NAME);
        System.out.format("E-mail: %s \n", EMAIL);
    }

}
